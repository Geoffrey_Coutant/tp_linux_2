# TP2 : Appréhender l'environnement Linux

## I. Service SSH

### 1. Analyse du service

***S'assurer que le service sshd est démarré***

```
radiou22@MacBook-Air-de-jojo ~ % ssh user1@192.168.64.59 -p 224
user1@192.168.64.59's password: 
Last login: Thu Dec  8 21:38:00 2022
[user1@linux1 ~]$
```


***Analyser les processus liés au service SSH***

```
[user1@linux1 ~]$ touch test
[user1@linux1 ~]$ nano test
```

```
bob a un chapeau rouge
emma surfe avec un dinosaure
eve a pas toute sa tête
```

```
[user1@linux1 ~]$ cat test | grep emma
emma surfe avec un dinosaure
[user1@linux1 ~]$ cat test | grep bob
bob a un chapeau rouge
[user1@linux1 ~]$ cat test | grep eve
eve a pas toute sa tête
```

***Déterminer le port sur lequel écoute le service SSH***

```
[user1@linux1 ~]$ sudo ss -alnpt | grep ssh
[sudo] password for user1: 
LISTEN 0      128          0.0.0.0:22        0.0.0.0:*    users:(("sshd",pid=695,fd=3))
LISTEN 0      128             [::]:22           [::]:*    users:(("sshd",pid=695,fd=4))
```

***Consulter les logs du service SSH***

```
[user1@linux1 log]$ sudo cat secure | tail -n10
Dec  8 11:49:23 linux1 sudo[861]: pam_unix(sudo:session): session closed for user root
Dec  8 12:18:07 linux1 sudo[926]:   user1 : TTY=pts/0 ; PWD=/home/user1 ; USER=root ; COMMAND=/sbin/ss -alnpt
Dec  8 12:18:07 linux1 sudo[926]: pam_unix(sudo:session): session opened for user root(uid=0) by user1(uid=1001)
Dec  8 12:18:07 linux1 sudo[926]: pam_unix(sudo:session): session closed for user root
Dec  8 12:22:25 linux1 sudo[954]:   user1 : TTY=pts/0 ; PWD=/var/log ; USER=root ; COMMAND=/bin/grep -r ssh
Dec  8 12:22:25 linux1 sudo[954]: pam_unix(sudo:session): session opened for user root(uid=0) by user1(uid=1001)
Dec  8 12:22:25 linux1 sudo[954]: pam_unix(sudo:session): session closed for user root
Dec  8 12:23:50 linux1 sudo[959]:   user1 : TTY=pts/0 ; PWD=/var/log ; USER=root ; COMMAND=/bin/cat secure
Dec  8 12:23:50 linux1 sudo[959]: pam_unix(sudo:session): session opened for user root(uid=0) by user1(uid=1001)
Dec  8 12:23:50 linux1 sudo[959]: pam_unix(sudo:session): session closed for user root
```

### 2. Modification du service

***Identifier le fichier de configuration du serveur SSH***
***Modifier le fichier de conf***

```
[user1@linux1 /]$ echo $RANDOM
224

[user1@linux1 ssh]$ sudo cat sshd_config | grep Port
Port 224

[user1@linux1 /]$ sudo firewall-cmd --add-port=224/tcp --permanent
success

[user1@linux1 /]$ sudo firewall-cmd --reload
success

[user1@linux1 /]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s1
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 224/tcp
  protocols: 
  forward: yes
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
  ```

***Redémarrer le service***

```
[user1@linux1 /]$ sudo systemctl restart sshd
```

***Effectuer une connexion SSH sur le nouveau port***

```
radiou22@Air-de-Geoffrey ~ % ssh user1@192.168.64.59 -p 224  
user1@192.168.64.59's password: 
Last login: Thu Dec  8 11:37:32 2022 from 192.168.64.1
[user1@linux1 ~]$ 
```

## II. Service HTTP

### 1. Mise en place

***Installer le serveur NGINX***

```
[user1@linux1 ~]$ nginx -version
nginx version: nginx/1.20.1
```

***Démarrer le service NGINX***

```
[user1@linux1 ~]$ sudo systemctl start nginx
[sudo] password for user1: 
```

***Déterminer sur quel port tourne NGINX***

```
[user1@linux1 ~]$ ss -alnpt
State     Recv-Q    Send-Q       Local Address:Port       Peer Address:Port    Process    
LISTEN    0         511                0.0.0.0:80              0.0.0.0:*                  
LISTEN    0         128                0.0.0.0:224             0.0.0.0:*                  
LISTEN    0         511                   [::]:80                 [::]:*                  
LISTEN    0         128                   [::]:224                [::]:*
```

nginx tourne sur le port 80.

***Déterminer les processus liés à l'exécution de NGINX***

```
[user1@linux1 ~]$ ss -alnpt | grep 80
LISTEN 0      511          0.0.0.0:80        0.0.0.0:*          
LISTEN 0      511             [::]:80           [::]:* 
```

***Euh wait***

```
radiou22@Air-de-Geoffrey ~ % curl 192.168.64.59 | head -n7  
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7620  100  7620    0     0   875k      0 --:--:-- --:--:-- --:--:-- 2480k
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
```

### 2. Analyser la conf de NGINX

***Déterminer le path du fichier de configuration de NGINX***

```
[user1@linux1 /]$ ls -al /etc/nginx/nginx.conf
-rw-r--r--. 1 root root 2334 Oct 31 16:37 /etc/nginx/nginx.conf
```

***Trouver dans le fichier de conf***

```
[user1@linux1 nginx]$ cat nginx.conf | grep server -a17

    server {
        listen       80;
        listen       [::]:80;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }
```

### 3. Déployer un nouveau site web

***Créer un site web***

```
[user1@linux1 var]$ sudo mkdir www
[user1@linux1 var]$ cd www
[user1@linux1 www]$ sudo mkdir tp2_linux
[user1@linux1 www]$ cd tp2_linux/
[user1@linux1 tp2_linux]$ sudo nano index.html
[user1@linux1 tp2_linux]$ cat index.html
<h1>MEOW mon premier serveur web</h1>
```

***Adapter la conf NGINX***

```
[user1@linux1 nginx]$ sudo nano nginx.conf
[user1@linux1 nginx]$ sudo systemctl restart nginx
[user1@linux1 nginx]$ cd conf.d/
[user1@linux1 conf.d]$ sudo nano tp2.conf
[user1@linux1 conf.d]$ echo $RANDOM
21270
[user1@linux1 conf.d]$ sudo nano tp2.conf

server {
  # le port choisi devra être obtenu avec un 'echo $RANDOM' là encore
  listen 21270;

  root /var/www/tp2_linux;
}

[user1@linux1 conf.d]$ sudo systemctl restart nginx

```

***Visitez votre super site web***

```
[user1@linux1 conf.d]$ sudo curl 192.168.64.59:21270
<h1>MEOW mon premier serveur web</h1>
```

## III. Your own services

### 2. Analyse des services existants

***Afficher le fichier de service SSH***

```
[user1@linux1 ~]$ systemctl status sshd | grep loaded
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
[user1@linux1 ~]$ cat /usr/lib/systemd/system/sshd.service | grep ExecStart
ExecStart=/usr/sbin/sshd -D $OPTIONS
```

***Afficher le fichier de service NGINX***

```
[user1@linux1 ~]$ cat /usr/lib/systemd/system/nginx.service | grep ExecStart=
ExecStart=/usr/sbin/nginx
```

### 3. Création de service

***Créez le fichier /etc/systemd/system/tp2_nc.service***

```
[user1@linux1 ~]$ echo $RANDOM
30907

[user1@linux1 ~]$ sudo nano /etc/systemd/system/tp2_nc.service
[sudo] password for user1: 

[user1@linux1 ~]$ cat /etc/systemd/system/tp2_nc.service

[Unit]
Description=Super netcat tout fou

[Service]
ExecStart=/usr/bin/nc -l 30907

```

***Indiquer au système qu'on a modifié les fichiers de service***

```
[user1@linux1 ~]$ sudo systemctl daemon-reload
[sudo] password for user1: 
```

***Démarrer notre service de ouf***

```
[user1@linux1 system]$ sudo systemctl start tp2_nc
```

***Vérifier que ça fonctionne***

```
[user1@linux1 system]$ systemctl status tp2_nc
● tp2_nc.service - Super netcat tout fou
     Loaded: loaded (/etc/systemd/system/tp2_nc.service; static)
     Active: active (running) since Thu 2022-12-08 16:26:11 CET; 11s ago
   Main PID: 1076 (nc)
      Tasks: 1 (limit: 5876)
     Memory: 732.0K
        CPU: 4ms
     CGroup: /system.slice/tp2_nc.service
             └─1076 /usr/bin/nc -l 30907

Dec 08 16:26:11 linux1 systemd[1]: Started Super netcat tout fou.

[user1@linux1 system]$ sudo ss -alnpt | grep nc
LISTEN 0      10           0.0.0.0:30907      0.0.0.0:*    users:(("nc",pid=1076,fd=4)) 
LISTEN 0      10              [::]:30907         [::]:*    users:(("nc",pid=1076,fd=3))
```

```
[user1@linux1#2 ~]$ nc 192.168.64.59 30907
hoooo
ppppp
```

```
[user1@linux1 ~]$ systemctl status tp2_nc
● tp2_nc.service - Super netcat tout fou
     Loaded: loaded (/etc/systemd/system/tp2_nc.service; static)
     Active: active (running) since Thu 2022-12-08 16:49:06 CET; 50min ago
   Main PID: 1104 (nc)
      Tasks: 1 (limit: 5876)
     Memory: 732.0K
        CPU: 5ms
     CGroup: /system.slice/tp2_nc.service
             └─1104 /usr/bin/nc -l 30907

Dec 08 16:49:06 linux1 systemd[1]: Started Super netcat tout fou.
Dec 08 17:00:32 linux1 systemd[1]: tp2_nc.service: Current command vanished from the unit>
Dec 08 17:38:47 linux1 nc[1104]: hoooo
Dec 08 17:38:48 linux1 nc[1104]: ppppp
```

***Les logs de votre service***

```
[user1@linux1 ~]$ sudo journalctl -xe -u tp2_nc | grep systemd
[sudo] password for user1: 
Dec 08 16:49:06 linux1 systemd[1]: Started Super netcat tout fou.

[user1@linux1 ~]$ sudo journalctl -xe -u tp2_nc | grep finished
[sudo] password for user1: 
░░ Subject: A start job for unit tp2_nc.service has finished successfully
░░ A start job for unit tp2_nc.service has finished successfully.

[user1@linux1 ~]$ nc 192.168.64.59 30907
Ncat: Connection refused.
```

***Affiner la définition du service***

```
[user1@linux1 ~]$ sudo nano /etc/systemd/system/tp2_nc.service
[user1@linux1 ~]$ sudo systemctl daemon-reload
[user1@linux1 ~]$ cat /etc/systemd/system/tp2_nc.service
[Unit]
Description=Super netcat tout fou

[Service]
ExecStart=/usr/bin/nc -l 30907
Restart=always
```
